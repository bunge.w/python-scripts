# uso: python3 main.py buscar pa la bras
# devuelve: listado a consola y dump de urls a links.txt

#https://github.com/alexmercerind/youtube-search-python
#from pprint import pprint
import sys
from youtubesearchpython import SearchVideos, SearchPlaylists

PAGE_SIZE=10
MAX_RESULTS=10
DUMP_LINKS='links.txt'

if DUMP_LINKS:
	dump_file=open(DUMP_LINKS, 'w+')
ids=set()

pagina=1
res=0
while 1:
	#offset=0=1
	videos = SearchVideos(' '.join(sys.argv[1:]), offset = pagina, mode = "dict", max_results = PAGE_SIZE)
	videosResult = videos.result()['search_result']

	if not videosResult:
		if pagina==0: print('Sin resultados')
		else: print('Fin resultados')
		break

	#pprint(videosResult)
	videosLen = len(videosResult)
	res += videosLen
	print(f'~~ {videosLen} resultados en página {pagina}:')
	for v in videosResult:
		title, views, link, id = v['title'], v['views'], v['link'], v['id']

		if id in ids:
			print(f'[ID duplicado {id}]')
			continue
		ids.add(id)

		print(f'{id} | {views:>10} | {title}')
		if DUMP_LINKS:
			#dump_file.write(f'{link}\n')
			dump_file.write(f'{link},{title}\n')

	if res >= MAX_RESULTS:
		print(f'Máxima cantidad de resultados alcanzada ({MAX_RESULTS})')
		break

	pagina+=1

if DUMP_LINKS:
	dump_file.close()
