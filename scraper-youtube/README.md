Buscador de links de videos de youtub

### Uso
```
source ./venv/bin/activate
pip install -r requirements.txt
python3 main.py mi busqueda
cat links.txt
```

### Configuración
En las primeras líneas de `main.py` están las variables:
- PAGE_SIZE = cantidad de resultados por página
- MAX_RESULTS = cantidad total de videos a buscar
- DUMP_LINKS = nombre de archivo a donde vuelca los resultados separados por coma

### Ejemplo
```
$ source ./venv/bin/activate
$ python3 main.py asd asd
~~ 10 resultados en página 1:
FZTN5izxI_U |    5236671 | ASD ASD ASD
a_C_swWMraI |      73325 | ASD - ASD
. . .
uBWCY0yF_5g |     302799 | Parenting a Family with Autism Spectrum Disorder (My Perfect Family: The Priestleys)
Máxima cantidad de resultados alcanzada (10)

$ cat links.txt
https://www.youtube.com/watch?v=FZTN5izxI_U,ASD ASD ASD
https://www.youtube.com/watch?v=a_C_swWMraI,ASD - ASD
. . .
https://www.youtube.com/watch?v=uBWCY0yF_5g,Parenting a Family with Autism Spectrum Disorder (My Perfect Family: The Priestleys)
```
