# telethon - https://github.com/LonamiWebs/Telethon
# telethon api - https://docs.telethon.dev/en/latest/quick-references/client-reference.html#client-ref
# iter_messages - https://docs.telethon.dev/en/latest/modules/client.html#telethon.client.messages.MessageMethods.iter_messages
from telethon import TelegramClient, events, sync
from os import environ as env

# generadas en https://my.telegram.org > API development tools
# se pueden pasar haciendo: ~$ API_ID=NN API_HASH=XX python3 scraper.py
# o setteándolas antes en la shell: ~$ export API_ID=NN API_HASH=XX
# en la primer corrida te va a pedir que verifiques con tu número de celu
api_id, api_hash = env['API_ID'], env['API_HASH']

client = TelegramClient('session_file', api_id, api_hash)
print('Start...', end='')
client.start()
print(' done start')

# listar mis chats 
#convs = client.get_dialogs()
##print(convs[0].stringify())
#for c in convs: print(c.date.strftime('%Y-%m-%d %H:%M%Z'), c.entity.id, c.name)

CHAT_URL='https://t.me/GRUPO'
CHAT_URL='https://t.me/CANAL'

# borrar mis últimos 5 msgs del canal
'''mi_id = client.get_me().id
print(mi_id)
mis_msgs = []
messages = client.iter_messages(CHAT_URL, from_user=mi_id)
for _ in range(3):
	m = next(messages)
	mis_msgs.append(m.id)
	
print(mis_msgs)
client.delete_messages(CHAT_URL, mis_msgs, revoke=True)'''


# mostrar ocurrencias de palabras
'''def print_msgs_count(search):
  # https://docs.telethon.dev/en/latest/modules/client.html#telethon.client.messages.MessageMethods.get_messages
	msgs = client.get_messages(CHAT_URL, 0, search=search)
	print(search, msgs.total)
[print_msgs_count(s) for s in ['palabra_1', 'palabra_N']]'''

# buscar links
import asyncio
import re
from telethon.tl.types import InputMessagesFilterUrl, MessageEntityUrl, MessageEntityTextUrl
async def print_msgs_count():
  # get_messages: https://docs.telethon.dev/en/latest/modules/client.html#telethon.client.messages.MessageMethods.get_messages
  # filtros: https://tl.telethon.dev/types/messages_filter.html
  msgs = await client.get_messages(CHAT_URL, 225, filter=InputMessagesFilterUrl)
  # devuelve de clase TotalList: https://docs.telethon.dev/en/latest/modules/helpers.html#telethon.helpers.TotalList
  error_malaentity = 0
  reg_ip = re.compile('^\d+(?:\.\d+)+(?::\d+)?$')
  reg_cleanurl = re.compile('^[\w-]+(?:[\.][\w/-]+)+$', re.IGNORECASE)
  reg_emojicode = re.compile('&#\d{6,};')
  urls = []
  urls_nohttp = []
  if len(msgs) == 0:
    print(f'No se encontró nada dentro de los {msgs.total} mensajes')
  else:
    print(f'{len(msgs)} mensajes con links encontrados')
    for m in msgs:
      # cada mensaje es de clase Message: https://docs.telethon.dev/en/latest/modules/custom.html#telethon.tl.custom.message.Message
      if len(m.entities) == 0:
        print('Sin entities, salteando')
        continue
      else:
        for e in m.entities:
          # tipos de entities: https://tl.telethon.dev/types/message_entity.html
          if isinstance(e, MessageEntityTextUrl):
            url = e.url
          elif isinstance(e, MessageEntityUrl):
            url = m.message[e.offset:e.offset+e.length]
          else:
            # hashtags, código, menciones, bold, phone, etc.
            #print(f'Salteando entity {e}')
            #error_malaentity += 1
            continue
          if not url:
            continue
          if reg_ip.match(url):
            continue
          if url.startswith('http'):
            urls.append(url)
            continue
          if reg_cleanurl.match(url):
            urls_nohttp.append(url)
            continue
          # encode: https://www.w3schools.com/python/ref_string_encode.asp
          mascii = m.message[:e.offset].encode('ascii', errors="xmlcharrefreplace").decode('ascii')
          nemojis = len(reg_emojicode.findall(mascii))
          url = m.message[e.offset-nemojis:e.offset-nemojis+e.length]
          if url.startswith('http'):
            urls.append(url)
            continue
          if reg_cleanurl.match(url):
            urls_nohttp.append(url)
            continue
          #print(nemojis)
          #print(mascii)
          print(f'Mala url: {url}')
        #endfor entities
      #return
    print(f'Se encontraron {len(urls)} urls')
    
    with open('dump.txt', 'w+') as file:
      file.write('\n'.join(urls))
      file.write('\n'.join(urls_nohttp))
    #endfor msgs
#print_msgs_count()
loop = asyncio.get_event_loop()
loop.run_until_complete(print_msgs_count())

# ver búsqueda de msj con su contexto
'''def print_msg_ctxt(search, ctxt_n):
	msg = client.get_messages(CHAT_URL, 1, search=search, reverse=True)
	msg_id = msg[0].id
	msgs = client.get_messages(CHAT_URL, min_id=msg_id-1-ctxt_n, max_id=msg_id+ctxt_n)
	[print(m.message) for m in msgs]
print_msg_ctxt('pato',3)'''


# ver últimos 5 msjs
'''usus = {}
messages = client.iter_messages(CHAT_URL)
for _ in range(5):
	m = next(messages)
	usuid = m.from_id
	if usuid in usus: usuname = usus[usuid]
	else: usuname = usus[usuid] = client.get_entity(usuid).username
	dispdate = m.date.strftime('%Y-%m-%d %H:%M%Z')
	print(dispdate, '|', usuname, '|', m.message)'''


### Primeros códigos testeados y andando
# tira stats sobre mi usu
#print(client.get_me().stringify())

# manda msj, no hizo falta el @
#client.send_message('usuarie', 'Hello! Talking to you from Telethon')

# se descarga mi foto
#client.download_profile_photo('me')

# se descarga un gif/imagen/archivo
#messages = client.get_messages('usuarie')
#messages[0].download_media()

# si me mandan un msj con ese pattern yo respondo (semibot)
#@client.on(events.NewMessage(pattern='(?i)hi|hello'))
#async def handler(event):
#    await event.respond('Hey!')
#print('Esperando msjs...')
#client.run_until_disconnected()
