#https://wiki.archlinux.org/index.php/Proxy_server#Using_a_SOCKS_proxy
#sudo pacman -S proxychains
#proxychains -q python3 main.py 

from itertools import chain
from pprint import pprint
from concurrent.futures import ThreadPoolExecutor
#pip install requests BeautifulSoup4 lxml
import requests
from bs4 import BeautifulSoup

THREADS_RUBROS = 5
THREADS_EMPRESAS = 10
CRAWL_RUBROS = 20
CRAWL_EMPRESAS = 50

crawled_rubros = 0
crawled_empresas = 0

# Función básica de hacer-request-y-parsear-html
def bsReq(url):
	r  = requests.get(url)
	data = r.text
	soup = BeautifulSoup(data, features="lxml")
	return soup

# Con esta clase scrapeamos las páginas de los
# rubros, buscando en sus resultados links de
# los sites de las empresas
class Rubro:
	def __init__(self, url):
		self.url = url
		
	def get_empresas(self):
		#print(f'Parseando {self.url}')
		#ejemplo: https://www.infoisinfo-ar.com/busqueda/abarrotes
		try:
			soup = bsReq(self.url)
		except requests.exceptions.ConnectionError as e:
			print(f'---- ERROR al conectarse con rubro {self.url}: {e}')
			return [ ]
		empresas_links = [ a.get('href') for a in soup.select('a.view-company') ]
		#print(empresas_links)		
		with ThreadPoolExecutor(max_workers=THREADS_EMPRESAS) as executor:
			posible_urls = executor.map(self.get_empresa_url, empresas_links[:CRAWL_EMPRESAS])		
		return [ url for url in list(posible_urls) if url is not None ]
			
	def get_empresa_url(self, page_url):
		global crawled_empresas
		#ejemplo: https://villa-luro.infoisinfo-ar.com/ficha/quimica-erpe-srl/228099
		try:
			soup = bsReq(page_url)
		except requests.exceptions.ConnectionError as e:
			print(f'---- ERROR al conectarse con empresa {page_url}: {e}')
			return None
		posible_link = soup.select('.mid-wrapper a.url')
		if posible_link:
			url = posible_link[0].string 
			if url == 'website':
				# la url está proxiada
				suburl = posible_link[0].get('href')
				try:
					url = self.get_empresa_from_suburl(suburl)
				except requests.exceptions.ConnectionError as e:
					print(f'---- ERROR al conectarse con empresa (suburl) {url}: {e}')
					return None
		else: 
			url = None
		crawled_empresas += 1
		print(f'+ Parseada empresa {crawled_empresas} {page_url}')
		return url
		
	def get_empresa_from_suburl(self, suburl):
		#ejemplo: https://www.infoisinfo-ar.com/rdir/Y2VudHJhbC1lbGVjdHJpY2EtZGUtbmVjb2NoZWEtLTI0Nzc4MS0tMA==
		soup = bsReq(suburl)
		posible_link = soup.select('main a')
		return posible_link[0].get('href') if posible_link else None
		

# Con esta clase inicializamos el scrapper
# con .crawl() buscá todos los links de los rubros
# y los manda a scrapear con la clase Rubros
class Infoisinfo:
	def __init__(self):
		self.url = 'https://www.infoisinfo-ar.com/listarubros'
		
	def crawl(self):
		soup = bsReq(self.url)
		rubros_links = [ a.get('href') for a in soup.select('.categories-grid a') ]
		#print(rubros_links)		
		with ThreadPoolExecutor(max_workers=THREADS_RUBROS) as executor:
			rubros_empresas = executor.map(self.get_rubro_empresas, rubros_links[:CRAWL_RUBROS])
		return list(chain(*rubros_empresas))
		
	def get_rubro_empresas(self, rubro_url):
		global crawled_rubros
		rubro = Rubro(rubro_url)
		empresas = rubro.get_empresas()
		crawled_rubros += 1
		print(f'+++ Parseado rubro {crawled_rubros} {rubro_url}')
		return empresas
		
print('Go!')

urls = Infoisinfo().crawl()
print(f'Links encontrados: {len(urls)}')

urls_face = 0
urls_wix = 0
urls_utiles = []
for url in urls:
	if url.find('facebook.com/') != -1: urls_face += 1
	elif url.find('.wixsite.com/') != -1: urls_wix += 1
	else: urls_utiles.append(url)
print(f'Face: {urls_face}, wix: {urls_wix}, utiles: {len(urls_utiles)}')
#pprint(urls_utiles)

with open('dump.txt','w+') as dump:
	dump.write('\n'.join(urls_utiles))
	dump.write('\n')
print('Links dumpeados')

print('Fin!')


# excódigo de hacer un timer que vaya mostrando progreso
'''from threading import Thread, Event

class StatusThread(Thread):
	def __init__(self, event):
		Thread.__init__(self)
		self.stopped = event
		self.last_crawled_rubros = -1
		self.last_crawled_empresas = -1

	def print_status(self):
		global crawled_rubros, crawled_empresas
		if self.last_crawled_rubros == crawled_rubros and self.last_crawled_empresas == crawled_empresas:
			print('.', end='', flush=True)
		else:
			print(f'\nProgreso: Rubros: {crawled_rubros}. Empresas: {crawled_empresas}', end='', flush=True)	
			self.last_crawled_rubros = crawled_rubros
			self.last_crawled_empresas = crawled_empresas

	def run(self):
		while not self.stopped.wait(1.0): self.print_status()
            
status_stop_flag = Event()
status_thread = StatusThread(status_stop_flag)
status_thread.start()
#status_stop_flag.set()'''
