import struct
import ipaddress

def _nbits_to_upackfmt(nbits):
	assert nbits%8 == 0
	assert 0 < nbits <= 32
	if nbits == 8: return 'B'
	elif nbits == 16: return 'H'
	elif nbits == 24: raise '3s'
	elif nbits == 32: return 'L'
	
def _make_struc_func(struct_bits, i, l):	
	assert i+l <= struct_bits
	if i == 0: #si esta al ppio
		def f(data): return data >> struct_bits-l	
	elif struct_bits-l == i: #si esta al final
		def f(data): return data & 2**l-1	
	else: #si esta en el medio
		def f(data): return (data >> struct_bits-(i+l)) & 2**l-1
	return f
		
class HeaderField:
	def __init__(self, display_name, bits):
		self.display_name = display_name
		self.bits = bits
		self.upack_struct_i = None
		self.upack_struct_func = None
		self.val = None
		
	def __repr__(self):
		return self.display_name + '=' + str(self.val)
		
	def __eq__(self, val):
		return self.val == val
	def __gt__(self, val):
		return self.val > val
		
'''class VarHeaderField():
	def __init__(self, display_name, typebits, typeval, lenbits=None, lenval=None):
		self.display_name = display_name
		self.typebits = typebits
		self.typeval = typeval
		self.lenbits = lenbits
		self.lenval = lenval'''

class ProtocolHeader:
	def __init__(self, min_words=None, fields=None):
		self.min_words = min_words
		self.min_bytes = min_words*4 #32bit-word=4bytes
		self.upack_format = None
		self.fields = fields
		for k in fields:
			assert k not in self.__dict__
			self.__dict__[k] = self.fields[k]
		self.optlenfield = None
		self.optbytes = None
		self._compile_structs()
	
	def _compile_structs(self):
		self.upack_format = '!'
		i = 0
		acumfields = []
		acumbits = 0
		for field in self.fields.values():
			field.upack_struct_i = i
			if acumbits == 0 and field.bits%8 == 0:
				self.upack_format += _nbits_to_upackfmt(field.bits)
				i += 1
			else:
				acumfields.append((field,acumbits))
				acumbits += field.bits
				assert acumbits <= 32
				field.upack_struct_i = i
				if acumbits%8 == 0:
					self.upack_format += _nbits_to_upackfmt(acumbits)
					i += 1
					for fild,byti in acumfields:
						fild.upack_struct_func = _make_struc_func(acumbits, byti, fild.bits)
					acumfields = []
					acumbits = 0
				
	def load_pkt(self, data):
		header_struct = struct.unpack(self.upack_format, data[:self.min_bytes])		
		for field in self.fields.values():
			if field.upack_struct_func:
				field.val = field.upack_struct_func(header_struct[field.upack_struct_i])
			else:
				field.val = header_struct[field.upack_struct_i]		
		if self.optlenfield and self.fields[self.optlenfield] > self.min_words:
			self.optbytes = (self.fields[self.optlenfield].val - self.min_words)*4
		else:
			self.optbytes = 0
		return self.min_bytes


class Protocol:	
	def __init__(self):
		self.protocol_name = None
		self.protocol_num = None
		self.header = None
		self.opts = None
	
	def process_packet(self, data):
		read_bytes = self.header.load_pkt(data)
		if self.header.optbytes:
			self.parse_options(self.header.optbytes, data[read_bytes:])
			read_bytes += self.header.optbytes;
		return read_bytes
		
	def parse_options(self, totbytes, data):
		raise NotImplementedError('Must implement parse_options')
		
	def debug_pkt(self):
		print( self.protocol_name + ' ' + ' '.join([str(hf) for hf in self.header.fields.values()]) )
		if self.header.optbytes:
			print( self.protocol_name + 'OPTS ' + ' '.join([opt[0] + ('' if not opt[1] else '='+str(opt[1])) for opt in self.opts]) )
						

class IP(Protocol):
	''' IPv4 - https://en.wikipedia.org/wiki/IPv4#Header
	 |0001020304050607|0809101112131415|1617181920212223|2425262728293031|
	1|Version | IHL   | DiffServ   |ECN|     Tot len bytes (head+data)   |          
	2|              Frag ID            |Flags |   Frag Offset            |
	3|     TTL        |    Protocol    |         Header chksum           |
	4|                             Source IP                             |
	5|                             Destin IP                             |
	[|Opt type|Opt len| 	  Opt data..           ..|..    paddding     |]
	'''
	def __init__(self):
		self.protocol_name = 'IP'
		self.protocol_num = 2048
		self.header = ProtocolHeader(
			min_words = 5,
			fields = {
				'ver': HeaderField('v', 4),
				'ihl': HeaderField('hlen', 4),
				'diffserv': HeaderField('dfcp', 6),
				'ecn': HeaderField('ecn', 2),
				'totlenbytes': HeaderField('tbytes', 16),
				'fragid': HeaderField('frgid', 16),
				'flag_res': HeaderField('flg_res', 1),
				'flag_df': HeaderField('flg_df', 1),
				'flag_mf': HeaderField('flg_mf', 1),
				'fragoffs': HeaderField('frgofs', 13),
				'ttl': HeaderField('ttl', 8),
				'protocol': HeaderField('prot', 8),
				'chksum': HeaderField('chksm', 16),
				'sourceip': HeaderField('srcip', 32),
				'destip': HeaderField('dstip', 32) } )
				#ip Option Format - http://www.tcpipguide.com/free/t_IPDatagramOptionsandOptionFormat.htm
			#IP Option Numbers - https://www.iana.org/assignments/ip-parameters/ip-parameters.xhtml
		'''self.header.optlenfield = 'ihl'
		self.header.opts = {
				'type': HeaderOptField('type', 4),
				'len': HeaderOptField('len', 4),
				'data': HeaderOptField('data', None, 'len') }'''
	
	def debug_pkt(self):
		print( 'IP ' + 
			' '.join([str(hf) for k,hf in self.header.fields.items() if k not in ['sourceip', 'destip']]) +
			' srcip=' + str(ipaddress.ip_address(self.header.sourceip.val)) + 
			' dstip=' + str(ipaddress.ip_address(self.header.destip.val)) )
		
	
class TCP(Protocol):
	''' TCP - http://www.omnisecu.com/tcpip/tcp-header.php
	 |0001020304050607|0809101112131415|1617181920212223|2425262728293031|
	1|            Source Port          |           Dest Port             |          
	2|                            Sequen Number                          |
	3|                             Acknow Num                            |
	4|Head Len |     Reserv     |UAPRSF|       Windw Size bytes          |
	5|             Checksum            |         Urg Pointer             |
	[|Opt type|Opt len| 	  Opt data..           ..|..    paddding     |]
	'''
	def __init__(self):
		self.protocol_name = 'TCP'
		self.protocol_num = 6
		self.header = ProtocolHeader(
			min_words = 5,
			fields = {
				'srcport': HeaderField('srcport', 16),
				'dstport': HeaderField('dstport', 16),
				'seqn': HeaderField('seqn', 32),
				'ackn': HeaderField('ackn', 32),
				'hl': HeaderField('hl', 4),
				'resrv': HeaderField('resrv', 6),
				'f_urg': HeaderField('f_urg', 1),
				'f_ack': HeaderField('f_ack', 1),
				'f_psh': HeaderField('f_psh', 1),
				'f_rst': HeaderField('f_rst', 1),
				'f_syn': HeaderField('f_syn', 1),
				'f_fin': HeaderField('f_fin', 1),
				'rcvwin': HeaderField('rcvwin', 16),
				'chck': HeaderField('chck', 16),
				'urgp': HeaderField('urgp', 16) } )
		self.header.optlenfield = 'hl'
		#TCP Options - https://www.freesoft.org/CIE/Course/Section4/8.htm				
		#TCP Option Kind Numbers - https://www.iana.org/assignments/tcp-parameters/tcp-parameters.xhtml
		self.opttypes = {0:'EOL', 1:'NOOP', 2:'MAXSS', 3:'WINSCL', 4:'SACKP', 8:'TIMESTP'}
		self.optparser = self.parse_option
				
	def parse_options(self, totbytes, data):
		self.opts = []
		read_opt_bytes = 0
		try:
			while read_opt_bytes < totbytes:
				opt_bytes = self.parse_option(data[read_bytes+read_opt_bytes:])
				
				
			
				opttype = data[0]	
				optname = self.opttypes.get(opttype)
				if not optname:
					raise Exception('unknown tcp option number ({})'.format(opttype))
					
				read_bytes = 1
				optval = ''
					
				if optname == 'EOL':
					read_bytes = 0
				elif optname == 'NOOP':
					pass
				else:
					optlen = data[1]
					read_bytes += 1
					if optname == 'MAXSS':
						if optlen != 4: raise Exception('tcp opt mss len != 4, es =' + str(optlen))
						optval = struct.unpack('!H' , data[read_bytes:read_bytes+2])[0]
						read_bytes += 2
					elif optname == 'WINSCL':
						#Window Scale - http://www.networksorcery.com/enp/protocol/tcp/option003.htm
						if optlen != 3: raise Exception('tcp opt winsca len != 3, es =' + str(optlen))
						optval = data[read_bytes]
						read_bytes += 1
					elif optname == 'SACKP':
						#Selective Acknowledgment - https://tools.ietf.org/html/rfc2018
						if optlen != 2: raise Exception('tcp opt sack len != 2, es =' + str(optlen))
						optval = data[read_bytes]
						read_bytes += 1
					elif optname == 'TIMESTP':
						#http://www.networksorcery.com/enp/protocol/tcp/option008.htm
						#https://cloudshark.io/articles/tcp-timestamp-option/
						if optlen != 10: raise Exception('tcp opt timestp len != 10, es =' + str(optlen))				
						optval = struct.unpack('!LL' , data[read_bytes:read_bytes+4*2])#(tsval, tsecr)
						read_bytes += 4*2
				
				self.opts.append((optname,optval))
				
				
				if opt_bytes == 0: break
				else: read_opt_bytes += opt_bytes
			read_bytes += tot_opt_bytes
		except Exception as e:
			print('@@@@@--ERR ' + str(e))
